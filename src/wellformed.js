'use strict';

const utils = require( './utils.js' );

const wellformedObject = ( zobject ) => {
	if ( zobject.Z1K1 === 'Z2' ) { // TODO
		return [];
	}
	return [ {
		Z1K1: 'Z5' // TODO
	} ];
};

const wellformedArray = ( arr ) => {
	// eslint-disable-next-line es-x/no-array-prototype-flat, no-use-before-define
	if ( arr.flatMap( wellformed ).length === 0 ) {
		return []; // TODO
	}
	return [ {
		Z1K1: 'Z5' // TODO
	} ];
};

const wellformed = ( zobject ) => {
	if ( utils.isObject( zobject ) ) {
		return wellformedObject( zobject );
	} else if ( utils.isArray( zobject ) ) {
		return wellformedArray( zobject );
	} else if ( utils.isString( zobject ) ) {
		return [];
	}
	return [ {
		Z1K1: 'Z5' // TODO
	} ];
};

exports.wellformed = wellformed;

'use strict';

const canonicalize = require( './canonicalize.js' );

const prettyprint = async ( zobject ) => JSON.stringify( await canonicalize.canonicalize( zobject ), null, '\t' );

const prettyprintAsync = async ( zobject ) => new Promise( ( resolve, reject ) => {
	resolve( prettyprint( zobject ) );
} );

exports.prettyprint = prettyprint;
exports.prettyprintAsync = prettyprintAsync;
